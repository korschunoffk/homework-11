# Dockerfile
ссылка на Dockerhub `korschunoffk/otus:latest`

Конфиги, докерфайл и index.html лежат в папке nginxhtml

```
FROM alpine:latest
RUN apk update && apk add nginx && adduser -D -g 'www' www \
&& mkdir /www && chown -R www:www /var/lib/nginx \
&& chown -R www:www /www \
&& rm  /etc/nginx/nginx.conf 
COPY nginx.conf /etc/nginx/nginx.conf 
COPY index.html /www/index.html
RUN mkdir -p /run/nginx
EXPOSE 80
CMD [ "nginx" ]
```

# Dockercompose

`docker-compose.yml`

Создал отдельные образы под Dockercompose

1. `korschunoffk/otus:v2`      - alpine nginx , конфиги в папке nginxphp
2. `korschunoffk/otusphp:v1`   - alpine php-7 , конфиги в папке dockerphp

```
version: '3.3'                              #Версия dockercompose

services:
    web:
        image: korschunoffk/otus:v2
        ports:
            - "80:80"
        volumes:
            - ./php:/www/                   #Что и куда пробрасываем (необходимо пробросить и в nginx и в php )
        networks:
            - code-network                  #Сеть
        depends_on:                         #Зависимости  (не запускалься пока не стартует php)
            - "php"
    php:
        image: korschunoffk/otusphp:v1
        ports: 
            - "9000:9000"
        volumes:
            - ./php:/www
        networks:
            - code-network

networks:
    code-network:
        driver: bridge
```
после равзорачивания стенда столкнулся с ошибкой 403 , решил командой  `sudo setenforce Permissive` 
